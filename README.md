## Objetivo do Framework

O Framework JSF tem por objetivo criar aplicações Java para Web, sendo assim a linguagem JSF utiliza componentes visuais quase prontos para que o desenvolvedor não se preocupe com JavaScript e HTML. Adicionando os componentes, eles serão exibidos em formato HTML. 
A especificação JSF define um conjunto de componentes de interface padrão e fornece uma interface de programação de aplicativos (API) para o desenvolvimento de components.JSF e permite a reutilização e extensão dos componentes de interface do usuário padrão existentes.

## Requisitos Básicos

JDK	1.5 ou superior.
Memória	- nenhum requisito mínimo.
Espaço em disco -	nenhum requisito mínimo.
Sistema operacional	 - nenhum requisito mínimo.

## Onde encontramos sua documentação? Ela existe em português? 

A documentação pode ser encontrada no site da Oracle, e a linguagem dela não é em português.
Link - http://www.oracle.com/technetwork/java/javaee/documentation/index-137726.html

## Instalação e Configuração

## 1º Passo: 
Você irá clonar o projeto, usando o comando: git clone git@gitlab.com:luizbuchvaitz/Projeto2-JSF.git
 
## 2º Passo:
Você precisará ter o XAMPP instalado, irá abrir ele e executar o MySQL, dentro do XAMPP irá selecionar a opção "Start" no Apache e MySQL e irá abrir a opção "admin" em MySQL,
logo após você irá ter que importar a tabela carros para poder acessar a tabela e listar os veículos e suas respectivas informações.

## 3º Passo:
Adicionar as bibliotecas "primefaces-5.1.jar", "Driver JDBC do MySQL" e "JSF 2.2" na biblioteca do projeto no NetBeans.
Obs: A biblioteca "primefaces-5.1.jar" terá que ser baixada no próprio site, pois a mesma que está disponível no netbeans está desatualizada.
Link para baixar biblioteca - (http://search.maven.org/remotecontent?filepath=org/primefaces/primefaces/5.1/primefaces-5.1.jar).

## Principais Vantagens

A linguagem JSF tem por vantagem a praticidade, isto quer dizer que a grande vantagem deste framework é que todo servidor de aplicações Java vem com implementação, sendo assim você não precisará se preocupar com alguns componentes visuais. E também tem como vantagem a legibilidade, fazendo com que o código fique de maneira simples de entender e bem organizada reduzindo o esforço na criação e manutenção de aplicativos que são executados em um servidor de aplicativos Java, facilitando o desenvolvimento de aplicações Web.

## Desvantagens do Framework durante o trabalho
O JSF tem como desvantagem usar ele sem bibliotecas, porém basta dar uma pesquisada e procurar entender sobre que ele fica bem mais produtivo.