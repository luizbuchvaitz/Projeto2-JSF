

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


/**
 *
 * @author Luiz Buchvaitz
 */
@ManagedBean
@SessionScoped
public class CarroCarro {
    
    private Carro carro = new Carro();
    private List<Carro> carros = new ArrayList<>();
    private CarroConecta carroConecta = new CarroConecta();     
    
    public void adicionar(){
        carros.add(carro);
        carroConecta.salvar(carro);
        carro = new Carro();
    }

      public void listar(){
          carros = carroConecta.buscar();
    }    
      
      public void editar(Carro c){
          carro = c;
      }
    
    public Carro getCarro() {
        return carro;
    }

    public void setCarro(Carro carro) {
        this.carro = carro;
    }

    public List<Carro> getCarros() {
        return carros;
    }

    public void setCarros(List<Carro> carros) {
        this.carros = carros;
    }
    
}
